/* eslint-disable no-console */
const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
  cors: {
    origin: 'http://127.0.0.1:9090',
    methods: ['GET', 'POST'],
    allowedHeaders: ['my-custom-header'],
    credentials: true
  }
});

const path = require('path');

app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '/index.html'));
});

io.on('connection', (socket) => {
  socket.emit('connection', socket.id);

  console.log(socket.id);
  socket.on('addMessage', (message) => {
    socket.broadcast.emit('sentMessage', message);
  });
});

server.listen(3001, () => {
  console.log('listening on port:3001');
});

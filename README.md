# Chatbot.io Fabien Fiette

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install chatbot.

Use node v16 use :
```bash
nvm install 16
```

```bash
npm i
```

## Client usage

Start the application dev with :

```bash
npm start
```

Created the dist with :

```bash
npm run dist
```

Analyse the coding rules with :

```bash
npm run lint
```
## Server usage

Start server in /server/ :

```bash
node server.js
```

## Command list

### !Help
Call `help` command of all bot

### Chuck Norris jokes bot
- `!chuck random` return a random jokes
- `!chuck randomCat {categories}` return a random jokes of the available categories
- `!chuck help` return the list of command of Chuck Norris jokes bot

*List of available categories (animal, career, celebrity, dev, explicit, fashion, food, history, money, movie, music, political, religion, science, sport, travel)*

### Translate bot
- `!tr yoda {text}` return your text translated to yoda language
- `!tr pirate {text}` return your text translated to pirate language
- `!tr help` return the list of command of Translate bot

### Command of Poke bot :
- `!poke stats {name or id}` return base stats of the pokemon
- `!poke type {type or id}` return the weakness of type
- `!poke help` return the list of command of poke bot
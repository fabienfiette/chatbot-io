/* eslint-disable no-console */
/* eslint-disable no-case-declarations */
import './index.scss';

const io = require('socket.io-client');

// Socket.io
const socket = io.connect('http://127.0.0.1:3001');

socket.on('connection', () => {
  console.log('Connected socket');
});

socket.on('connect', () => {
  console.log('ok');
});

class Chat {
  constructor(id) {
    this.root = document.getElementById('root');
    this.messages = [];
    this.contacts = [{
      name: 'Fabien',
      avatar: 'https://i.ibb.co/pwCbM2V/YEP-Emote-Banner.jpg',
      contactId: 0
    },
    {
      name: 'Chuck Norris Bot',
      avatar: 'https://i.ibb.co/5xpr9GP/Chuck-norris.jpg',
      contactId: 1
    },
    {
      name: 'Translate Bot',
      avatar: 'https://i.ibb.co/b6DF0P9/translate.png',
      contactId: 2
    },
    {
      name: 'Poke Bot',
      avatar: 'https://i.ibb.co/J3TmhhQ/pokeBot.png',
      contactId: 3
    }
    ];
    this.chatId = id;
  }

  addUser(user) {
    const { name, avatar } = user;
    this.contacts.push({
      name,
      avatar: avatar || 'https://sbcf.fr/wp-content/uploads/2018/03/sbcf-default-avatar.png',
      contactId: this.contacts.length
    });

    localStorage.setItem('contact', JSON.stringify(this.contacts));

    this.renderContacts(this.contacts);
  }

  event() {
    document.getElementById(`${this.chatId}-form`).addEventListener('submit', (e) => {
      const chatInput = document.getElementById(`${this.chatId}-input`);
      if (!chatInput.value.length <= 0) {
        this.addMessage({
          authorId: 0,
          bot: false,
          text: chatInput.value
        });
        chatInput.value = '';
      }
      e.preventDefault();
    });

    document.getElementById(`${this.chatId}-button`).addEventListener('click', () => {
      const chatInput = document.getElementById(`${this.chatId}-input`);
      if (!chatInput.value.length <= 0) {
        this.addMessage({
          authorId: 0,
          bot: false,
          text: chatInput.value
        });
        chatInput.value = '';
      }
    });
  }

  async fetchApi(url) {
    const x = await fetch(url)
      .then((res) => res.json())
      .then((json) => json);
    return x;
  }

  async caseMessage(message) {
    const { text } = message;

    if (text.slice(0, 1) === '!') {
      const commandBot = text.split(' ');
      switch (commandBot[0].slice(1, commandBot[0].length)) {
        // Vérifier quelle bot appeller
        case 'poke':
          // Commande pour l'api pokemon
          switch (commandBot[1]) {
            case 'stats':
              const { stats, forms } = await this.fetchApi(`https://pokeapi.co/api/v2/pokemon/${commandBot[2]}`);
              this.addMessage({
                authorId: 3,
                bot: true,
                text: `Base stats of ${forms[0].name} :</br>HP : ${stats[0].base_stat}</br>Attack : ${stats[1].base_stat}</br>Defense : ${stats[2].base_stat}</br>Attack Spe : ${stats[3].base_stat}</br>Defense Spe : ${stats[4].base_stat}</br>Speed Spe : ${stats[5].base_stat}`
              });
              break;
            case 'type':
              const pokeTypeApi = await this.fetchApi(`https://pokeapi.co/api/v2/type/${commandBot[2]}`);
              const dmgDouble = pokeTypeApi.damage_relations.double_damage_from;
              this.addMessage({
                authorId: 3,
                bot: true,
                text: `Type ${pokeTypeApi.name} is weak against :</br>${dmgDouble.map((obj) => `- ${obj.name}</br>`).join('')}`
              });
              break;
            case 'help':
              this.addMessage({
                authorId: 3,
                bot: true,
                text: 'Command of poke bot :</br>- \'!poke stats {name or id}\' return base stats of the pokemon</br>- \'!poke type {type or id}\' return the weakness of type</br>- \'!poke help\' return the list of command of poke bot'
              });
              break;
            default:
              this.addMessage({
                authorId: 3,
                bot: true,
                text: 'Invalid command'
              });
              break;
          }
          break;
        case 'chuck':
          // Commande du bot Chuck Norris
          switch (commandBot[1]) {
            case 'random':
              const chuckApiRdm = await this.fetchApi('https://api.chucknorris.io/jokes/random');
              this.addMessage({
                authorId: 1,
                bot: true,
                text: `${chuckApiRdm.value}`
              });
              break;
            case 'randomCat':
              const chuckApiRdmCat = await this.fetchApi(`https://api.chucknorris.io/jokes/random?category=${commandBot[2]}`);
              this.addMessage({
                authorId: 1,
                bot: true,
                text: `${chuckApiRdmCat.value}`
              });
              break;
            case 'help':
              const chuckCatList = await this.fetchApi('https://api.chucknorris.io/jokes/categories');
              this.addMessage({
                authorId: 1,
                bot: true,
                text: `Command of Chuck Norris jokes bot :</br>- '!chuck random' return a random jokes</br>- '!chuck randomCat {categories}' return a random jokes of the available categories</br>List of available categories :</br>(${chuckCatList.map((obj) => `${obj}`)})</br>- '!chuck help' return the list of command of Chuck Norris jokes bot`
              });
              break;
            default:
              this.addMessage({
                authorId: 1,
                bot: true,
                text: 'Invalid command'
              });
              break;
          }
          break;
        case 'tr':
          // Commande du bot Translate
          switch (commandBot[1]) {
            case 'yoda':
              const yodaApi = await this.fetchApi(`https://api.funtranslations.com/translate/yoda.json?text=${commandBot.slice(2, commandBot.length).join(' ')}`);
              if (yodaApi.error) {
                this.addMessage({
                  authorId: 2,
                  bot: true,
                  text: `${yodaApi.error.message}`
                });
                return;
              }
              this.addMessage({
                authorId: 2,
                bot: true,
                text: `Yoda :</br>${yodaApi.contents.translated}`
              });
              break;
            case 'pirate':
              const pirateApi = await this.fetchApi(`https://api.funtranslations.com/translate/pirate.json?text=${commandBot.slice(2, commandBot.length).join(' ')}`);
              if (pirateApi.error) {
                this.addMessage({
                  authorId: 2,
                  bot: true,
                  text: `${pirateApi.error.message}`
                });
                return;
              }
              this.addMessage({
                authorId: 2,
                bot: true,
                text: `Orc :</br>${pirateApi.contents.translated}`
              });
              break;
            case 'help':
              this.addMessage({
                authorId: 2,
                bot: true,
                text: 'Command of Translate bot :</br>- \'!tr yoda {text}\' return your text translated to yoda language</br>- \'!tr pirate {text}\' return your text translated to pirate language</br>- \'!tr help\' return the list of command of Translate bot'
              });
              break;
            default:
              this.addMessage({
                authorId: 2,
                bot: true,
                text: 'Invalid command'
              });
              break;
          }
          break;
        // Liste de toutes les commandes
        case 'help':
          // Commande help du bot Chuck Norris
          const chuckCatList = await this.fetchApi('https://api.chucknorris.io/jokes/categories');
          this.addMessage({
            authorId: 1,
            bot: true,
            text: `Command of Chuck Norris jokes bot :</br>- '!chuck random' return a random jokes</br>- '!chuck randomCat {categories}' return a random jokes of the available categories</br>List of available categories :</br>(${chuckCatList.map((obj) => `${obj}`)})</br>- '!chuck help' return the list of command of Chuck Norris jokes bot`
          });
          // Commande du bot Translate
          this.addMessage({
            authorId: 2,
            bot: true,
            text: 'Command of Translate bot :</br>- \'!tr yoda {text}\' return your text translated to yoda language</br>- \'!tr pirate {text}\' return your text translated to pirate language</br>- \'!tr help\' return the list of command of Translate bot'
          });
          // Commande help du poke bot
          this.addMessage({
            authorId: 3,
            bot: true,
            text: 'Command of poke bot :</br>- \'!poke stats {name or id}\' return base stats of the pokemon</br>- \'!poke type {type or id}\' return the weakness of type</br>- \'!poke help\' return the list of command of poke bot'
          });
          break;
        default:
          break;
      }
    }
  }

  addMessage(message) {
    const { bot, text, authorId } = message;
    const date = new Date();
    const dateFormat = `${date.getHours()}:${date.getMinutes()} ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;

    const messagesArray = this.messages;
    messagesArray.push({
      authorId,
      bot,
      text,
      date: dateFormat
    });

    localStorage.setItem('message', JSON.stringify(this.messages));

    const messenger = document.getElementById(this.chatId);

    messenger.innerHTML += this.renderMessage(messagesArray[messagesArray.length - 1]);

    if (!bot) {
      this.caseMessage(message);
    }

    messenger.scrollTop = messenger.scrollHeight;
  }

  renderMessage(message) {
    const {
      authorId, bot, text, date
    } = message;
    const contact = this.contacts.map((obj) => obj.contactId).indexOf(authorId);
    const { name, avatar } = this.contacts[contact];

    let classSend = false;
    if (authorId === 0) {
      classSend = true;
    }

    return `
        <div class='${classSend ? 'send' : 'receive'} d-flex mb-2'>
            <div class='avatar ${bot ? 'bot' : ''} ms-2 me-2'>
                <img src=${avatar} alt='avatar' class='rounded-circle'>
            </div>
            <div class='d-flex flex-column'>
                <span class='message-name h6'>${name}</span>
                <div class='message card border-0'>
                    <div class='card-body'>
                        <span>${text}</span>
                    </div>
                </div>
                <span class='message-date'>${date}</span>
            </div>
        </div>
        `;
  }

  renderChat() {
    return `
        <!-- Chat -->
        <div class='container-fluid bg-dark-default p-4'>
            <div id=${this.chatId} class='chat d-flex flex-column'>

                <!-- Message -->
                ${this.messages.map((obj) => this.renderMessage(obj)).join('')}

            </div>
            <form id='${`${this.chatId}-form`}' class='input-chat d-flex pt-4'>
                <input id='${this.chatId}-input' type='text' class='form-control me-3' autocomplete='off' placeholder='Start chatting' aria-describedby='${this.chatId}-button'>
                <button class='btn btn-purpose' id='${this.chatId}-button' type='button'>Send</button>
            </form>
        </div>
        `;
  }

  renderContact(contact) {
    const { avatar, name } = contact;
    return `
        <div class='user d-flex align-items-center rounded mb-2'>
            <div class='avatar ms-2 me-2'>
                <img src='${avatar}' alt='avatar' class='rounded-circle'>
            </div>
            <span>${name}</span>
        </div>
        `;
  }

  renderContacts(contacts) {
    return `
        <!-- Side bar -->
        <div class='collapse collapse-horizontal bg-dark-drawer' id='navbarToggleExternalContent'>
            <div class='drawer fit-content d-flex flex-column p-2'>

                ${contacts.map((obj) => this.renderContact(obj)).join('')}

            </div>
        </div>
        `;
  }

  render() {
    if (JSON.parse(localStorage.getItem('contact'))) {
      this.contacts = JSON.parse(localStorage.getItem('contact'));
    }
    if (JSON.parse(localStorage.getItem('message'))) {
      this.messages = JSON.parse(localStorage.getItem('message'));
    }
    this.root.innerHTML += this.renderContacts(this.contacts);
    this.root.innerHTML += this.renderChat();
    this.event();
    const messenger = document.getElementById(this.chatId);

    messenger.scrollTop = messenger.scrollHeight;
  }
}

// localStorage.clear();

const chat = new Chat('C1F');

chat.render();

// chat.addMessage({
//     'authorId' : 0,
//     'bot': false,
//     'text': 'Bonjour'
// })

// chat.addMessage({
//     'authorId': 2,
//     'bot': true,
//     'text': 'Hi !'
// })

// chat.addMessage({
//     'authorId': 1,
//     'bot': true,
//     'text': 'Hello'
// })

// chat.addMessage({
//     'authorId': 3,
//     'bot': true,
//     'text': 'poke'
// })
